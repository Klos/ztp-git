package com.libraryserver.springserver.services;

import com.libraryserver.springserver.beans.Role;
import com.libraryserver.springserver.beans.User;

import java.util.HashMap;

public class UserService {
    private static HashMap<String, User> usersList = new HashMap<String, User>();

    static{
        usersList.put("admin", new User("admin", "admin", Role.ADMIN));
        usersList.put("user", new User("user", "user"));
        usersList.put("user2", new User("user1", "user"));
    }

    public static User getUser(String login)
    {
        return usersList.get(login);
    }

    public static boolean checkUser(String username, String password) {
        if (username != null
                && password != null
                && usersList.containsKey(username)) {
            return password.equals(usersList.get(username));
        }
        return false;
    }
}
