package com.libraryserver.springserver.services;

import com.google.gson.Gson;
import com.libraryserver.springserver.beans.Role;
import com.libraryserver.springserver.beans.User;
import com.libraryserver.springserver.repositories.UsersRepository;
import com.libraryserver.springserver.requests.LoginRequest;
import com.libraryserver.springserver.responses.LoginResponse;
import com.libraryserver.springserver.responses.Response;
import org.springframework.stereotype.Component;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Base64;

@Component
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginService extends HttpServlet {
	public void loginUser(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		System.out.println("Parse Login Data");
		LoginRequest loginRequest = parseData(request, response);
		System.out.println("\nLogin");
		login(request, response, loginRequest);
	}

	private String getBase64FromString(String str) {
		return Base64.getEncoder().encodeToString(str.getBytes());
	}

	private boolean checkIfUserCorrectPassword(String username, String password) {
		System.out.println("Check User credentials");
		UsersRepository usersRepository = new UsersRepository();
		String correctPassword = usersRepository.getPasswordByUsername(username);
		return correctPassword != null && correctPassword.equals(password);
	}

	private LoginRequest parseData(HttpServletRequest request, HttpServletResponse response) throws IOException {
		LoginRequest loginRequest = null;
		Gson gson = new Gson();
		response.setContentType("application/json;charset=UTF-8");
		try {
			loginRequest = gson.fromJson(request.getReader(), LoginRequest.class);
		} catch (IOException ex) {
			System.out.println("Parse data not successful");
			Response exResponse = new Response();
			exResponse.setMessage("Parse data not successful");
			exResponse.setStatus(500);
			response.setStatus(500);
			gson.toJson(exResponse, response.getWriter());
		}
		if (loginRequest != null) {
			System.out.println("password: " + loginRequest.getPassword());
			System.out.println("username: " + loginRequest.getUsername());
		} else {
			Response exResponse = new Response();
			exResponse.setMessage("Gson did not parse credentials");
			exResponse.setStatus(500);
			response.setStatus(500);
			gson.toJson(exResponse, response.getWriter());
		}
		return loginRequest;
	}

	private void login(HttpServletRequest request, HttpServletResponse response, LoginRequest loginRequest)
			throws IOException {
		boolean isLoginSuccessful;
		String login = loginRequest.getUsername();
		String password = loginRequest.getPassword();
		Role role = "admin".equals(loginRequest.getUsername()) ? Role.ADMIN : Role.USER;
		isLoginSuccessful = checkIfUserCorrectPassword(login, password);
		Gson gson = new Gson();
		if (isLoginSuccessful) {
			System.out.println("User logged in successfully");
			User user = new User(login, password, role);
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			String encodedLogin = getBase64FromString(login);
			response.addCookie(new Cookie("userId", encodedLogin));
			response.setContentType("application/json;charset=UTF-8");
			LoginResponse loginResponse = new LoginResponse();
			loginResponse.setRole(role);
			gson.toJson(loginResponse, response.getWriter());

		} else {
			System.out.println("User login failed");
			response.setContentType("application/json;charset=UTF-8");
			Response exResponse = new Response();
			exResponse.setMessage("User login failed");
			exResponse.setStatus(400);
			response.setStatus(400);
			response.addCookie(new Cookie("userId", "InvalidCredentials"));
			gson.toJson(exResponse, response.getWriter());
		}
	}
}