package com.libraryserver.springserver.filters;

import com.google.gson.Gson;
import com.libraryserver.springserver.beans.User;
import com.libraryserver.springserver.responses.Response;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;

@Order(2)
public class CookieFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException {
		HttpServletRequest req = (HttpServletRequest) request;
		try {
//			Object userObj = req.getSession().getAttribute("loggedUser");
			Object userObj = req.getSession().getAttribute("user");
			if (userObj == null) {
				throw new Exception("Unauthorized user");
			}
			User user = (User) userObj;
			System.out.println(user);
			if (!checkForUserIdCookie(req.getCookies(), user)) {
				throw new Exception("No proper cookie");
			}
			chain.doFilter(request, response);
		} catch (Exception ex) {
			Gson gson = new Gson();
			response.setContentType("application/json;charset=UTF-8");
			Response exResponse = new Response();
			exResponse.setMessage(ex.getLocalizedMessage());
			exResponse.setStatus(401);
			((HttpServletResponse) response).setStatus(401);
			gson.toJson(exResponse, response.getWriter());
		}
	}

	private boolean checkForUserIdCookie(Cookie[] cookies, User user) {
		for (Cookie cookie : cookies) {
			if ("userId".equals(cookie.getName())) {
				return new String(Base64.getDecoder()
						.decode(cookie.getValue().getBytes()))
						.equals(user.getUsername());
			}
		}
		return false;
	}

	@Override
	public void destroy() {
	}
}