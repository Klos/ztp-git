package com.libraryserver.springserver.controller;

import com.libraryserver.springserver.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class LoginController {
	private LoginService loginService;

	@PostMapping(path = "/login")
	public void login(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		loginService.loginUser(request, response);
	}

	@Autowired
	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}
}
