package com.libraryserver.springserver.responses;

import com.libraryserver.springserver.beans.Role;

public class LoginResponse {
	private Role role;

	public LoginResponse() {
	}

	public LoginResponse(Role role) {
		this.role = role;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}