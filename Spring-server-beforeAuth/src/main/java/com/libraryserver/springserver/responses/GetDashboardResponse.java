package com.libraryserver.springserver.responses;

import com.libraryserver.springserver.beans.Book;

import java.util.List;

public class GetDashboardResponse {
	private List<Book> books;
	private Book book;
	private int status;

	public GetDashboardResponse() {
	}

	public GetDashboardResponse(List<Book> books, int status) {
		this.books = books;
		this.status = status;
	}

	public GetDashboardResponse(Book book, int status) {
		this.book = book;
		this.status = status;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
