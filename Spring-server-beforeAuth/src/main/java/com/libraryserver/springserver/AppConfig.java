package com.libraryserver.springserver;

import com.libraryserver.springserver.filters.CookieFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
	@Bean
	public FilterRegistrationBean<CookieFilter> cookieFilter() {
		FilterRegistrationBean<CookieFilter> registrationBean = new FilterRegistrationBean<>();
		registrationBean.setFilter(new CookieFilter());
		registrationBean.addUrlPatterns("/dashboard", "/dashboard*", "/dashboard/*");
		return registrationBean;
	}
}