package com.libraryserver.springserver.repositories;

import java.util.HashMap;
import java.util.Map;

public class UsersRepository {
	private static Map<String, String> usersCredentials;

	static {
		usersCredentials = new HashMap<>();
		usersCredentials.put("user", "user");
		usersCredentials.put("user1", "user");
		usersCredentials.put("user2", "user");
		usersCredentials.put("admin", "admin");
	}

	public String getPasswordByUsername(String username) {
		return usersCredentials.get(username);
	}
}
