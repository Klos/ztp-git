package com.libraryserver.springserver.repositories;

import com.libraryserver.springserver.beans.Book;

import java.util.ArrayList;
import java.util.List;

public class BooksRepository {
	private static List<Book> books;

	static {
		books = new ArrayList<>();
		books.add(new Book("Adam Mickiewicz", "Pan Tadeusz"));
		books.add(new Book("Henryk Sienkiewicz", "Krzyżacy"));
		books.add(new Book("Juliusz Słowacki", "Balladyna"));
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		BooksRepository.books = books;
	}
}