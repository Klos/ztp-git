/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.librarygr2.servlets;

import com.google.gson.Gson;
import com.library.librarygr2.requests.AddBookRequest;
import com.library.librarygr2.beans.Book;
import com.library.librarygr2.requests.RemoveBookRequest;
import com.library.librarygr2.helpers.HtmlHelpers;
import com.library.librarygr2.responses.DeleteDashboardResponse;
import com.library.librarygr2.responses.ExceptionResponse;
import com.library.librarygr2.responses.GetDashboardResponse;
import com.library.librarygr2.responses.PostDashboardResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ewadpaw
 */
@WebServlet(name = "DashboardServlet", urlPatterns = {"/dashboard/*"})
public class DashboardServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Book> books = (List<Book>) request.getServletContext().getAttribute("books");
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println(HtmlHelpers.getHTMLHead());
            out.println(getBooksAsDivs(books));
            out.println(HtmlHelpers.getLogoutButton(request.getContextPath()));
            out.println(HtmlHelpers.getHTMLFoot());
        }
    }

    private String getBooksAsDivs(List<Book> books) {
        StringBuilder sb = new StringBuilder();
        for (Book book : books) {
            sb.append("<div>\n")
                    .append("<p>Autor: ").append(book.getAuthor()).append(" ")
                    .append("Tytuł: ").append(book.getTitle()).append("</p>")
                    .append("</div>");
        }

        return sb.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("In DashboardServlet GET");
        response.setContentType("application/json;charset=UTF-8");
        Gson gson = new Gson();
        try {
            List<Book> books = getBooksFromContext(request.getServletContext());
            GetDashboardResponse res = new GetDashboardResponse(books, 200);
            gson.toJson(res, response.getWriter());
        } catch (Exception ex) {
            ExceptionResponse exResponse = new ExceptionResponse();
            exResponse.setMessage(ex.getLocalizedMessage());
            exResponse.setStatus(500);
            response.setStatus(500);
            gson.toJson(exResponse, response.getWriter());
        }
        System.out.println("Out DashboardServlet GET");
    }

    private List<Book> getBooksFromContext(ServletContext servletContext) {
        return (List<Book>) servletContext.getAttribute("books");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("In DashboardServlet POST");
        response.setContentType("application/json;charset=UTF-8");
        Gson gson = new Gson();
        try {
            AddBookRequest addBookRequest = null;
            try {
                addBookRequest = gson.fromJson(request.getReader(), AddBookRequest.class);
            }
            catch (Exception ex) {
                ExceptionResponse exResponse = new ExceptionResponse();
                exResponse.setMessage(ex.getLocalizedMessage());
                exResponse.setStatus(500);
                response.setStatus(500);
                gson.toJson(exResponse, response.getWriter());
            }

            List<Book> books = (List<Book>) request.getServletContext().getAttribute("books");

            String title = addBookRequest.getTitle();
            String author = addBookRequest.getAuthor();

            if (!"".equals(title) && !"".equals(author)) {
                Book book = new Book(author, title);
                books.add(book);
                PostDashboardResponse res = new PostDashboardResponse(book, 200);
                gson.toJson(res, response.getWriter());
            }
            else {
                ExceptionResponse exResponse = new ExceptionResponse();
                exResponse.setMessage("Empty author or title sent.");
                exResponse.setStatus(400);
                response.setStatus(400);
                gson.toJson(exResponse, response.getWriter());
            }
        } catch (Exception ex) {
            ExceptionResponse exResponse = new ExceptionResponse();
            exResponse.setMessage(ex.getLocalizedMessage());
            exResponse.setStatus(500);
            response.setStatus(500);
            gson.toJson(exResponse, response.getWriter());
        }
        System.out.println("Out DashboardServlet POST");
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("In DashboardServlet DELETE");
        response.setContentType("application/json;charset=UTF-8");
        Gson gson = new Gson();
        try {
            List<Book> books = (List<Book>) request.getServletContext().getAttribute("books");
            int index = Integer.parseInt(request.getPathInfo().substring(1));
            Book bookToRemove = null;
            for (Book b : books) {
                if (b.getId() == index) {
                    bookToRemove = b;
                }
            }
            if (bookToRemove != null) {
                DeleteDashboardResponse res = new DeleteDashboardResponse(bookToRemove, 200);
                books.remove(bookToRemove);
                gson.toJson(res, response.getWriter());
            } else {
                ExceptionResponse exResponse = new ExceptionResponse();
                exResponse.setMessage("Book not found in books list");
                exResponse.setStatus(400);
                response.setStatus(400);
                gson.toJson(exResponse, response.getWriter());
            }
        } catch (Exception ex) {
            ExceptionResponse exResponse = new ExceptionResponse();
            exResponse.setMessage(ex.getLocalizedMessage());
            exResponse.setStatus(500);
            response.setStatus(500);
            gson.toJson(exResponse, response.getWriter());
        }
        System.out.println("Out DashboardServlet DELETE");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>





}
