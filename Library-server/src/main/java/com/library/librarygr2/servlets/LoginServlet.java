/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.librarygr2.servlets;

import com.google.gson.Gson;
import com.library.librarygr2.requests.LoginRequest;
import com.library.librarygr2.beans.Role;
import com.library.librarygr2.beans.User;
import com.library.librarygr2.helpers.StringHelpers;
import com.library.librarygr2.responses.ExceptionResponse;
import com.library.librarygr2.responses.LoginResponse;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

/**
 *
 * @author ewadpaw
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("In LoginServlet");

        Gson gson = new Gson();
        LoginRequest loginRequest = null;
        try {
            loginRequest = gson.fromJson(request.getReader(),LoginRequest.class);
        }
        catch (Exception ex) {
            ExceptionResponse exResponse = new ExceptionResponse();
            exResponse.setMessage(ex.getLocalizedMessage());
            exResponse.setStatus(500);
            response.setStatus(500);
            gson.toJson(exResponse, response.getWriter());
        }

        if (loginRequest == null)
            System.out.println("loginRequest = null");
        else
        {
            if (loginRequest.getUsername() != null)
                System.out.println("login =" + loginRequest.getUsername());
            else
                System.out.println("login = null");
            if (loginRequest.getPassword() != null)
                System.out.println("Password =" + loginRequest.getPassword());
            else
                System.out.println("Password = null");
        }


        if (checkUser(loginRequest.getUsername(), loginRequest.getPassword())) {
            User user = new User(loginRequest.getUsername(), loginRequest.getPassword(), Role.USER);

            if (isAdmin(loginRequest))
              user.setRole(Role.ADMIN);
            HttpSession session = (HttpSession) request.getSession(true);
            if (session == null)
            {
                LoginResponse loginResponse = new LoginResponse();
                loginResponse.setMessage("Session is null");
                loginResponse.setStatus(500);
                response.setStatus(500);
                gson.toJson(loginResponse, response.getWriter());
            }
            session.setAttribute("loggedUser", user);
            String userIdBase64 = StringHelpers.getBase64FromString(loginRequest.getUsername());
            response.addCookie(new Cookie("userId", userIdBase64));

            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setRole(user.getRole());
            loginResponse.setStatus(200);
            response.setStatus(200);
            loginResponse.setMessage("Logged in successfully");

            gson.toJson(loginResponse, response.getWriter());

        }
        else {
            // bad request
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setMessage("Wrong login credentials");
            loginResponse.setStatus(400);
            response.setStatus(400);
            gson.toJson(loginResponse, response.getWriter());

        }

        System.out.println("Out LoginServlet");
    }

    private boolean isAdmin(LoginRequest loginRequest) {
        if ("admin".equals(loginRequest.getUsername()))
            return true;
        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private boolean checkUser(String username, String password) {
        HashMap<String, String> usersList = new HashMap<String, String>();
        usersList.put("admin", "admin");
        usersList.put("user", "user");
        usersList.put("user2", "user");
        usersList.put("user4", "user");
        usersList.put("user3", "user");

        if (username != null
                && password != null
                && usersList.containsKey(username)) {
            return password.equals(usersList.get(username));
        }
        return false;
    }
}



