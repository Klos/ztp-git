package com.library.librarygr2.requests;

public class RemoveBookRequest {
    private String author;
    private String title;
    private int index;

    public RemoveBookRequest() {
    }

    public RemoveBookRequest(String author, String title) {
        this.author = author;
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
