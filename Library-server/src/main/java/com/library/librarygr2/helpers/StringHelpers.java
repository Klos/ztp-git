package com.library.librarygr2.helpers;

import java.util.Base64;

public class StringHelpers {
    public static String getBase64FromString(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes());
    }
}
