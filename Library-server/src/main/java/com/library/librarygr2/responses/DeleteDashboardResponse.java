package com.library.librarygr2.responses;

import com.library.librarygr2.beans.Book;

public class DeleteDashboardResponse {
    private Book book;
    private int status;
    public DeleteDashboardResponse(Book book, int status) {
        this.book = book;
        this.status = status;
    }
}
