package com.library.librarygr2.responses;

import com.library.librarygr2.beans.Book;

import java.util.List;

public class PostDashboardResponse {
    private Book book;
    private int status;
    public PostDashboardResponse(Book book, int status) {
        this.book = book;
        this.status = status;
    }
}
