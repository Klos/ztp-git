package com.library.librarygr2.responses;

import com.library.librarygr2.beans.Role;

public class LoginResponse extends Response {
    private Role role;

    public LoginResponse() {
    }

    public LoginResponse(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
