import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ErrorIcon from '@material-ui/icons/Error';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';

 const ToastNotifiaction = ({ onClose, open }) => (
  <Snackbar
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    open={open}
    autoHideDuration={6000}
    onClose={onClose}
    ContentProps={{
      'aria-describedby': 'message-id',
    }}
  >
    <SnackbarContent
        aria-describedby="client-snackbar"
        message={
          <span id="client-snackbar">
            <ErrorIcon />
            Blad
          </span>
        }
        action={[
          <IconButton
          key="close"
          aria-label="close"
          color="inherit"
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>,
        ]}
    />
  </Snackbar>
);
 
 ToastNotifiaction.propTypes = {
   onClose: PropTypes.func.isRequired,
   open: PropTypes.bool,
 }

export default ToastNotifiaction;