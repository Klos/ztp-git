/*
 * ToastNotifiaction Messages
 *
 * This contains all the text for the ToastNotifiaction component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.ToastNotifiaction';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ToastNotifiaction component!',
  },
});
