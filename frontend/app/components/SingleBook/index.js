import React from 'react';
import PropTypes from 'prop-types';

// material ui
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';

const SingleBook = ({ dense, title, author, deleteBook, isAdmin }) => {
  return (
    <Grid item xs={12} md={6}>   
      <List dense={dense}>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <FolderIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={title}
            secondary={author}
          />
          <ListItemSecondaryAction>
            {isAdmin && 
              <IconButton aria-label="Delete" onClick={deleteBook}>
                <DeleteIcon />
              </IconButton>
            }
          </ListItemSecondaryAction>
        </ListItem>
      </List>
    </Grid>
  );
}

SingleBook.propTypes = {
  author: PropTypes.string.isRequired,
  deleteBook: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  dense: PropTypes.bool,
  secondary: PropTypes.bool,
};

SingleBook.defaultProps = {
  dense: false,
  secondary: false,
};

export default SingleBook;
