import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';

// material ui components
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import messages from './messages';
import useStyles from './styles';

const AddBookForm = ({ intl, onSubmit }) => {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          {intl.formatMessage(messages.addBook)}
        </Typography>
        <form noValidate onSubmit={onSubmit} method="POST">
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="title"
            label={intl.formatMessage(messages.title)}
            name="title"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="author"
            label={intl.formatMessage(messages.author)}
            type="author"
            id="author"
            autoComplete="current-author"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {intl.formatMessage(messages.add)}
          </Button>
        </form>
      </div>
    </Container>
  );
}

AddBookForm.propTypes = {
  intl: intlShape.isRequired,
  onSubmit: PropTypes.func.isRequired,
}
export default injectIntl(AddBookForm);
