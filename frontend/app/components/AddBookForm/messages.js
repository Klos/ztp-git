/*
 * AddBookForm Messages
 *
 * This contains all the text for the AddBookForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.AddBookForm';

export default defineMessages({
  addBook: {
    id: `${scope}.addBook`,
    defaultMessage: 'Dodaj ksiazke',
  },
  add: {
    id: `${scope}.add`,
    defaultMessage: 'Dodaj!',
  },
  title: {
    id: `${scope}.title`,
    defaultMessage: 'Tytuł',
  },
  author: {
    id: `${scope}.author`,
    defaultMessage: 'Autor',
  },
});
