import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

const ButtonComp = ({ text }) => <Button color="primary" variant="contained" type="submit">{text}</Button>;

ButtonComp.propTypes = {
  text: PropTypes.string.isRequired,
};

export default ButtonComp;
