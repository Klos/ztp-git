import { SET_BOOKS, ADD_BOOK, DELETE_BOOK } from './constants';
import apiFetch from 'utils/apiFetch';

export const getBooks = () => (dispatch) => (
  apiFetch('/dashboard', dispatch, null, 'GET')
    .then((books) => {
      dispatch({ type: SET_BOOKS, books });
    })
)

export const addBook = (event) => (dispatch) => {
  event.preventDefault();
  const title = event.target.elements.title.value;
  const author = event.target.elements.author.value;

  apiFetch('/dashboard', dispatch, { title, author }, 'POST')
    .then(({ book }) => {
      dispatch({ type: ADD_BOOK, book })
    })
}

export const deleteBook = (book) => (dispatch) => {
  apiFetch(`/dashboard/${book.id}`, dispatch, null, 'DELETE')
    .then(() => {
      dispatch({ type: DELETE_BOOK, book })
    })
}















// fucntion deleteBook(book) {\  
//   return function(dispatch) {
//     apiFetch
//   }
// }