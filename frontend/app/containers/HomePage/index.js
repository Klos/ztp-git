import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import SingleBook from 'components/SingleBook';
import AddBookForm from 'components/AddBookForm';
import { compose } from 'redux';
import { getBooks, addBook, deleteBook } from './actions';
import makeSelectHomePage from './selectors';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class HomePage extends Component {

  componentDidMount() {
    this.props.getBooks();
  }

  deleteBook(book) {
    this.props.deleteBook(book);
  }

  render() {
    return (
      <div>
        <Grid container spacing={10} justify="center" alignItems="center">
          <Grid item xs={8}>
            <h1>
              <FormattedMessage {...messages.header} />
            </h1>
          </Grid>
          <Grid item xs={8}>
            {this.props.isAdmin ? <AddBookForm onSubmit={this.props.addBook} /> : null}
            <Typography component="h1" variant="h5">
              Lista Ksiazek
          </Typography>
            {
              this.props.books.map((book, index) =>
                <SingleBook
                  key={book.id}
                  author={book.author}
                  title={book.title}
                  isAdmin={this.props.isAdmin}
                  deleteBook={() => this.deleteBook(book)}
                />)
            }
          </Grid>
        </Grid>
      </div>
    )
  }
}

HomePage.propTypes = {
  books: PropTypes.arrayOf(PropTypes.object),
  classes: PropTypes.object.isRequired,
  getBooks: PropTypes.func.isRequired,
  addBook: PropTypes.func.isRequired,
  isAdmin: PropTypes.bool,
};

HomePage.defultProps = {
  isAdmin: false,
  books: [],
}

const mapDispatchToProps = {
  getBooks,
  addBook,
  deleteBook,
};

const withConnect = connect(
  makeSelectHomePage,
  mapDispatchToProps,
);

export default (withStyles(styles)(compose(withConnect)(HomePage)));
