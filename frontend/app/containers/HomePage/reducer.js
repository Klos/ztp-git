/*
 *
 * homePage reducer
 *
 */
import produce from 'immer';
import { SET_BOOKS, ADD_BOOK, DELETE_BOOK } from './constants';

export const initialState = {
  books: [],
};

/* eslint-disable default-case, no-param-reassign */
const homePageReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
      case SET_BOOKS:
        return {
          ...state,
          books: action.books.books,
        }
      case ADD_BOOK:
        return {
          ...state,
          books: [
            ...state.books,
            action.book
          ]
        }
      case DELETE_BOOK:
        return {
          ...state,
          books: state.books.filter((book) => book.id !== action.book.id)
        }
    }
  });


export default homePageReducer;
