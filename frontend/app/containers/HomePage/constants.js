/*
 *
 * HomePage constants
 *
 */

export const SET_BOOKS = 'app/HomePage/SET_BOOKS';
export const ADD_BOOK = 'app/HomePage/ADD_BOOK';
export const DELETE_BOOK = 'app/HomePage/DELETE_BOOK';