/**
 *
 * LoginPage
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Grid from '@material-ui/core/Grid';

import LoginForm from 'components/LoginForm';

import makeSelectLoginPage from './selectors';
import {
  withRouter
} from 'react-router-dom'

import { login } from './actions';

class LoginPage extends Component {
  goLogin = (event) => {
    this.props.login(event).then(() => {
      this.props.history.push("/dashboard");
    })
  }

  render() {
    return (
      <Grid container spacing={10} justify="center" alignItems="center">
        <Grid item xs={8}>
          <LoginForm onSubmit={this.goLogin} />
        </Grid>
      </Grid>
    )
  }
}

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
    login,
  };

const withConnect = connect(
  makeSelectLoginPage,
  mapDispatchToProps,
);

export default withRouter(compose(withConnect)(LoginPage));
