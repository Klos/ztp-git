import apiFetch from 'utils/apiFetch';
import { SET_ADMIN, SET_API_CALL_ERROR } from './constants';

 const setAdmin = () => ({
    type: SET_ADMIN,
  });

export const login = (event) => (dispatch) => {
  event.preventDefault();
  const username = event.target.elements.username.value;
  const password = event.target.elements.password.value;

  return apiFetch('/login', dispatch, { username, password })
  .then((response) => {
    if (response.role === 'ADMIN') {
      dispatch(setAdmin());
    }apiFetch
  });
}

export const setApiCallError = () => ({
  type: SET_API_CALL_ERROR,
  open: true,
})
