import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';

import HomePage from 'containers/HomePage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import ToastNotifiaction from 'components/ToastNotifiaction'

import { setApiCallError as onClose } from 'containers/LoginPage/actions';
import makeSelectApp from './selectors';
import GlobalStyle from '../../global-styles';

const App = ({ open, onClose }) => (
  <div>
    <Switch>
      <Route exact path="/dashboard" component={HomePage} />
      <Route exact path="/" component={LoginPage} />
      <Route component={NotFoundPage} />
    </Switch>
    <ToastNotifiaction open={open} onClose={onClose} />
    <GlobalStyle />
  </div>
);

App.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool,
}

App.defaultProps = {
  open: false,
}

const mapDispatchToProps = {
  onClose,
};

const withConnect = connect(
  makeSelectApp,
  mapDispatchToProps,
);

export default compose(withConnect)(App);
