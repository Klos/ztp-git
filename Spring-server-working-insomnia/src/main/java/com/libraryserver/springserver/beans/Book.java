package com.libraryserver.springserver.beans;

public class Book {
	private String author;
	private String title;
	private int id;
	private static int counter = 0;

	public Book(String author, String title) {
		this.author = author;
		this.title = title;
		this.id = getNextId();
	}

	public Book(int id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public int getId() {
		return id;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setId() {
		this.id = getNextId();
	}

	private int getNextId() {
		counter++;
		return counter;
	}
}
