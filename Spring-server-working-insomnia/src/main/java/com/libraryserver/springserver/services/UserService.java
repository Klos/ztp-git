package com.libraryserver.springserver.services;

import com.libraryserver.springserver.beans.User;
import org.springframework.stereotype.Service;

import java.util.HashMap;

import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService{
    private static final HashMap<String, User> users = new HashMap<>();

    public UserService() {
        String[] userRoles = {"USER"};
        String[] adminRoles = {"USER", "ADMIN"};
        users.put("user", new User("user", "user", userRoles));
        users.put("admin", new User("admin", "admin", adminRoles));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = findUserbyUsername(username);

        UserBuilder builder = null;
        if (user != null) {
            builder = org.springframework.security.core.userdetails.User.withUsername(username);
            builder.password(new BCryptPasswordEncoder().encode(user.getPassword()));
            builder.roles(user.getRoles());
        } else {
            throw new UsernameNotFoundException("User not found.");
        }

        return builder.build();
    }

    public User findUserbyUsername(String username) {
        return users.get(username);
    }
}
