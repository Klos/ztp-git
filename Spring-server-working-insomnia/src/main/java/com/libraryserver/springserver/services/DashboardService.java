package com.libraryserver.springserver.services;

import com.libraryserver.springserver.beans.Book;
import org.springframework.stereotype.Component;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.util.ArrayList;
import java.util.List;

@Component
@WebServlet(name = "DashboardServlet", urlPatterns = {"/dashboard", "/dashboard*", "/dashboard/*"})
public class DashboardService extends HttpServlet {

	private static List<Book> books;

	static {
		books = new ArrayList<>();
		books.add(new Book("Adam Mickiewicz", "Pan Tadeusz"));
		books.add(new Book("Henryk Sienkiewicz", "Krzyżacy"));
		books.add(new Book("Juliusz Słowacki", "Balladyna"));
	}

	public List<Book> getBooks() {
		return books;
	}
	public void setBooks(List<Book> books) {
		DashboardService.books = books;
	}

	public static void addBook(Book book){
		books.add(book);
	}

	public static Book deleteBook(int id){
		Book bookToRemove = null;
		for (Book b : books) {
			if (b.getId() == id) {
				bookToRemove = b;
				books.remove(bookToRemove);
				break;
			}
		}
		return bookToRemove;
	}

	public static Book getBook(int id)
	{
		Book book = null;
		for (Book b : books) {
			if (b.getId() == id) {
				book = b;
			}
		}
		return book;
	}
}
