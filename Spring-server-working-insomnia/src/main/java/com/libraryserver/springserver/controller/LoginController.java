package com.libraryserver.springserver.controller;


import com.libraryserver.springserver.beans.User;
import com.libraryserver.springserver.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	private UserService userService;

	@PostMapping()
	public String[] login(Authentication authentication) {
		User user = userService.findUserbyUsername(authentication.getName());
		return user.getRoles();
	}

}
