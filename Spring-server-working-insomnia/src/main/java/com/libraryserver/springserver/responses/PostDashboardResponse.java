package com.libraryserver.springserver.responses;

import com.libraryserver.springserver.beans.Book;

public class PostDashboardResponse {
    private Book book;
    private int status;
    public PostDashboardResponse(Book book, int status) {
        this.book = book;
        this.status = status;
    }
}
