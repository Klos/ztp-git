package com.libraryserver.springserver.responses;

import com.libraryserver.springserver.beans.Book;

public class DeleteDashboardResponse {
    private Book book;
    private int status;

    public DeleteDashboardResponse() {
    }

    public DeleteDashboardResponse(Book book, int status) {
        this.book = book;
        this.status = status;
    }
}
