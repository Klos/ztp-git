package com.libraryserver.springserver.controller;


import com.libraryserver.springserver.beans.User;
import com.libraryserver.springserver.dto.CredentialsDto;
import com.libraryserver.springserver.dto.UserDto;
import com.libraryserver.springserver.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	private UserService userService;

	@PostMapping()
	public UserDto login(@RequestBody CredentialsDto credentials) {
		try {
			User user = userService.findUserbyUsername(credentials.getUsername());
			if (user != null && user.getPassword().equals(credentials.getPassword())) {
				return toUserDto(user);
			} else {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid password");
			}
		} catch (UsernameNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid username", e);
		}
	}

	private UserDto toUserDto(User user) {
		return new UserDto(
				user.getUsername(),
				user.getRoles()
		);
	}

}

