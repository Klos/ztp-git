package com.libraryserver.springserver.controller;

import com.google.gson.Gson;
import com.libraryserver.springserver.beans.Book;
import com.libraryserver.springserver.requests.AddBookRequest;
import com.libraryserver.springserver.responses.*;
import com.libraryserver.springserver.services.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class DashboardController {
	private DashboardService dashboardService;

	@GetMapping(path = "/dashboard")
	public GetDashboardResponse getAllBooks(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		response.setContentType("application/json;charset=UTF-8");
		Gson gson = new Gson();
		try {
			List<Book> books = dashboardService.getBooks();
			return new GetDashboardResponse(books, 200);
		} catch (Exception ex) {
			ExceptionResponse exResponse = new ExceptionResponse();
			exResponse.setMessage(ex.getLocalizedMessage());
			exResponse.setStatus(500);
			response.setStatus(500);
			gson.toJson(exResponse, response.getWriter());
		}
		return null;
	}

	@GetMapping(path = "/dashboard/{bookId}")
	public GetDashboardResponse getBook(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer bookId)
			throws IOException {
		response.setContentType("application/json;charset=UTF-8");
		Gson gson = new Gson();
		try {
			Book book = dashboardService.getBook(bookId);
			return new GetDashboardResponse(book, 200);
		} catch (Exception ex) {
			ExceptionResponse exResponse = new ExceptionResponse();
			exResponse.setMessage(ex.getLocalizedMessage());
			exResponse.setStatus(500);
			response.setStatus(500);
			gson.toJson(exResponse, response.getWriter());
		}
		return null;
	}

	@PostMapping(path = "/dashboard")
	public void addNewBook(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		response.setContentType("application/json;charset=UTF-8");
		Gson gson = new Gson();
		try {
			AddBookRequest addBookRequest = null;
			try {
				addBookRequest = gson.fromJson(request.getReader(), AddBookRequest.class);
			}
			catch (Exception ex) {
				ExceptionResponse exResponse = new ExceptionResponse();
				exResponse.setMessage(ex.getLocalizedMessage());
				exResponse.setStatus(500);
				response.setStatus(500);
				gson.toJson(exResponse, response.getWriter());
			}

			String title = addBookRequest.getTitle();
			String author = addBookRequest.getAuthor();

			if (!"".equals(title) && !"".equals(author)) {
				Book newBook = new Book(author, title);
				DashboardService.addBook(newBook);
				PostDashboardResponse res = new PostDashboardResponse(newBook, 200);
				gson.toJson(res, response.getWriter());
			}
			else {
				ExceptionResponse exResponse = new ExceptionResponse();
				exResponse.setMessage("Empty author or title sent.");
				exResponse.setStatus(400);
				response.setStatus(400);
				gson.toJson(exResponse, response.getWriter());
			}
		} catch (Exception ex) {
			ExceptionResponse exResponse = new ExceptionResponse();
			exResponse.setMessage(ex.getLocalizedMessage());
			exResponse.setStatus(500);
			response.setStatus(500);
			gson.toJson(exResponse, response.getWriter());
		}
	}

	@DeleteMapping(path = "/dashboard/{bookId}")
	public void deleteBookFromLibrary(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer bookId)
			throws IOException {
		response.setContentType("application/json;charset=UTF-8");
		Gson gson = new Gson();
		try {
			Book deletedBook = dashboardService.deleteBook(bookId);

			if (deletedBook != null) {
				DeleteDashboardResponse res = new DeleteDashboardResponse(deletedBook, 200);
				gson.toJson(res, response.getWriter());
			} else {
				ExceptionResponse exResponse = new ExceptionResponse();
				exResponse.setMessage("Book not found in books list");
				exResponse.setStatus(400);
				response.setStatus(400);
				gson.toJson(exResponse, response.getWriter());
			}
		} catch (Exception ex) {
			ExceptionResponse exResponse = new ExceptionResponse();
			exResponse.setMessage(ex.getLocalizedMessage());
			exResponse.setStatus(500);
			response.setStatus(500);
			gson.toJson(exResponse, response.getWriter());
		}
	}

	@Autowired
	public void setDashboardService(DashboardService dashboardService) {
		this.dashboardService = dashboardService;
	}
}