package com.libraryserver.springserver.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class UserDto {

    private final String username;
    private final String[] roles;

    @JsonCreator
    public UserDto(@JsonProperty("username") String username, @JsonProperty("role") String[] roles) {
        this.username = username;
        this.roles = roles;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("roles")
    public String[] getRoles() {
        return roles;
    }
}