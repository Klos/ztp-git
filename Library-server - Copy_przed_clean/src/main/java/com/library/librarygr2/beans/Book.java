/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.librarygr2.beans;

/**
 *
 * @author Filip
 */
public class Book {
    private static int counter = 0;
    private final String author;
    private final String title;
    private final int id;

    public Book(String author, String title) {
        this.author = author;
        this.title = title;
        this.id = getNextId();
    }
    
    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

    private int getNextId() {
        counter++;
        return counter;
    }
}
