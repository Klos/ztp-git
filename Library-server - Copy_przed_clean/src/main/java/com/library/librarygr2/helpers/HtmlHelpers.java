/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.librarygr2.helpers;

/**
 *
 * @author ewadpaw
 */
public class HtmlHelpers {
      public static String getHTMLHead() {
        StringBuilder sb = new StringBuilder();

        sb.append("<!DOCTYPE html>");
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<title>Servlet DashboardServlet</title>");
        sb.append("</head>");
        sb.append("<body>");

        return sb.toString();
    }

    public static String getHTMLFoot() {
        StringBuilder sb = new StringBuilder();

        sb.append("</body>");
        sb.append("</html>");

        return sb.toString();
    }  
    
    public static String getLogoutButton() {
        StringBuilder sb = new StringBuilder();

        sb.append("<div><form action=\"./logout\">");
        sb.append("<button type=\"submit\">Wyloguj</button>");
        sb.append("</form></div>");
        
        return sb.toString();
    }
    
    public static String getLogoutButton(String contextURI) {
        StringBuilder sb = new StringBuilder();

        sb.append("<div><form action=\"");
        sb.append(contextURI);
        sb.append("/logout\">");
        sb.append("<button type=\"submit\">Wyloguj</button>");
        sb.append("</form></div>");
        
        return sb.toString();
    }
}
